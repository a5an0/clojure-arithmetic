# exp-parse

A little program that evaluates arithmetic expressions.

Internally, it uses `instaparse` to parse the expression, builds an RPN representation of the expression, and then evaluates the RPN form.

Not performant, optimized, or even all that useful. Mostly just wanted to play with instaparse.

## Installation

`lein run` or `lein uberjar` if you want a standalone.

## Usage

    $ java -jar exp-parse-0.1.0-standalone.jar [expression]

Throw the `-s` flag to see the RPN stack as it is evaluated. Handy!

If you want to use whitespace, like `1 + 2 + 3`, make sure you surround it in quotes, or bash will split up the expression.
    $ java -jar exp-parse-0.1.0-standalone.jar "1 + 2 + 3"

## Examples

    $ java -jar exp-parse-0.1.0-standalone.jar 1+2+3
    1+2+3 = 6

### Bugs

Probably a **TON**. Here are some I know about:
* Not using TCO, so with a really big expression, you can stack overflow
* no usage text being printed out


## License

Do literally whatever you want with it.
