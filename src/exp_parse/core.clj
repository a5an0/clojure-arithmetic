(ns exp-parse.core
  (:require [instaparse.core :as insta]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.walk :refer [postwalk postwalk-demo]])
  (:gen-class))

(def SHOW_STACK (atom false))

;; grammar adapted from http://www.math-cs.gordon.edu/courses/cps222/examples-2015/Recursion/grammar.pdf
(def arithmetic
  (insta/parser
   "EXPR = TERM | EXPR ADD TERM | EXPR MINUS TERM
    TERM = FACTOR | TERM MUL FACTOR | TERM DIV FACTOR
    FACTOR = NUMBER | MINUS FACTOR | PAREN EXPR PAREN
    PAREN = '(' | ')'
    ADD = '+'
    MINUS = '-'
    MUL = '*'
    DIV = '/'
    NUMBER = #'[0-9]+'"
   :auto-whitespace :standard))

(defn- arr-to-rpn [acc expr]
  (let [node (first expr)
        children (rest expr)]
    (case node
      (:EXPR :TERM) (if (= (count children) 3)
                      ;; if there are 3 parts, then the middle one is the operator
                      ;; so do that one last to get us to RPN ordering
                      (conj acc
                            (arr-to-rpn acc (first children))
                            (arr-to-rpn acc (nth children 2))
                            (arr-to-rpn acc (second children)))
                      (conj acc (arr-to-rpn acc (first children))))
      :FACTOR (case (first (first children))
                :NUMBER (arr-to-rpn acc (first children))
                :PAREN (arr-to-rpn acc (-> children rest first)) ;; eval the middle term (not the parens)
                :MINUS (conj [] (arr-to-rpn acc (-> children rest first)) -1 "*") ; append a [-1 *]
                )
      :ADD "+"
      :MINUS "-"
      :MUL "*"
      :DIV "/"
      :NUMBER (Integer. (second expr))
      ))
  )

(defn- convert-expr-to-stack [expr]
  (->> expr arithmetic (arr-to-rpn []) flatten))

(defn- eval-rpn [stack expr]
  ;; global variable in clojure. muahahaha!!!
  (if @SHOW_STACK (println "> stack is" stack))
  (if (= 0 (count expr))
    (first stack)
    (let [sym (first expr)]
      (if (contains? #{"+" "-" "*" "/"} sym)
        (let [rside (-> stack peek)
              lside (-> stack pop peek)
              popped_stack (-> stack pop pop)
              op (case sym
                   "+" +
                   "-" -
                   "*" *
                   "/" /)
              result (apply op [lside rside])]
          (eval-rpn (conj popped_stack result) (rest expr)))
        (eval-rpn (conj stack sym) (rest expr))
        )
      )
    )
  )

(defn evaluate-arithmetic
  "Evaluates an arbitrary arithmetic expression."
  [expression]
  (eval-rpn [] (convert-expr-to-stack expression)))

(def cli-options
  [["-s" "--show-stack" "Show RPN Stack as it's evaluated"]])

(defn -main
  [& args]
  (let [opts (parse-opts args cli-options)
        expression (first (opts :arguments))]
    (if (nil? expression)
      (println "Provide an arithmetic expression")
      (do
        (if (-> opts :options :show-stack) (reset! SHOW_STACK true))
        (println expression "=" (evaluate-arithmetic expression)))
      )
    )
  )
