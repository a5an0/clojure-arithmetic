(ns exp-parse.core-test
  (:require [clojure.test :refer :all]
            [exp-parse.core :refer :all]))

(deftest basic-addition
  (testing "Adding some numbers"
    (is (= 15 (evaluate-arithmetic "1+2+3+4+5")))))

(deftest basic-operations
  (testing "simple expression with basic operations"
    (is (= -5/2 (evaluate-arithmetic "1+2*3/4-5")))))

(deftest complex-expression
  (testing "complex expresion"
    (is (= -209 (evaluate-arithmetic "20*30/5+14-9*27-100")))))

(deftest paren-use
  (testing "using parens"
    (is (= 244/5 (evaluate-arithmetic "4+2*(1+3)/5*7*(2+7-5)")))))

(deftest negative-multiplication
  (testing "multiplication with negative numbers"
    (is (= -4 (evaluate-arithmetic "2*-2")))
    (is (= -4 (evaluate-arithmetic "-2*2")))
    (is (= 4 (evaluate-arithmetic "-2*-2")))))

(deftest all-operations
  (testing "expression with all operations"
    (is (= -49832 (evaluate-arithmetic "-17+43*(42-65+21*-54)-11+90*11/-(23-2*7)/11-43")))))

(deftest whitespace
  (testing "whitespace is allowed by the parser"
    (is (= 4 (evaluate-arithmetic "2 + 2")))
    (is (= 4 (evaluate-arithmetic "    2 +       2")))
    (is (= 6 (evaluate-arithmetic " 2 + 2 + 2 ")))
    ))
